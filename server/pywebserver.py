from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import threading, mimetypes, os, re, hashlib, urllib, sys, shutil, json, cgi, hashlib, random
import handlerextension

def read_in_chunks(filepath):
    global CHUNKLENGTH
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    file=open(filepath,"rb")
    while True:
        data = file.read(CHUNKLENGTH)
        if not data:
            file.close()
            break
        yield data

def parse_byte_range(byte_range):
    '''Returns the two numbers in 'bytes=123-456' or throws ValueError.
    The last number or both numbers may be None.
    '''
    BYTE_RANGE_RE = re.compile(r'bytes=(\d+)-(\d+)?$')
    if (not byte_range is None) and (not byte_range.strip() == ''):
        m = BYTE_RANGE_RE.match(byte_range)
    else:
        return None,None

    if not m:
        raise ValueError('Invalid byte range %s' % byte_range)

    first, last = [x and int(x) for x in m.groups()]
    if last and last < first:
        raise ValueError('Invalid byte range %s' % byte_range)
    return first, last

class Handler(BaseHTTPRequestHandler):

    def handleWrite(self,bytes):
        try:
            self.wfile.write(bytes)
        except (ConnectionAbortedError, ConnectionResetError):
            # print("Connection canceled for '"+self.client_address[0]+"'")
            pass

    def handleStatus(self,statuscode):
        try:
            self.send_response(statuscode)
        except (ConnectionAbortedError, ConnectionResetError):
            # print("Connection canceled for '"+self.client_address[0]+"'")
            pass

    def handleHeader(self,field,value):
        try:
            self.send_header(field,value)
        except (ConnectionAbortedError, ConnectionResetError):
            # print("Connection canceled for '"+self.client_address[0]+"'")
            pass

    def handleHeaderEnd(self):
        try:
            self.end_headers()
        except (ConnectionAbortedError, ConnectionResetError):
            # print("Connection canceled for '"+self.client_address[0]+"'")
            pass

    def setGlobal(self,name,value):
        globals()[name]=value
        return

    def getGlobal(self,name):
        return globals()[name]

    def executePythonFile(self):
        filepath=self.translatePath()
        if os.path.isfile(filepath):
            with open(filepath,"r") as f:
                code=f.read()
            exec(code)
            return True
        else:
            return False

    def isAuthenticated(self):
        global LOCAL_IP_RANGE, HASH_SECRET
        m=hashlib.blake2b()
        m.update(self.client_address[0].encode())
        m.update(HASH_SECRET.encode())
        token=m.hexdigest()
        if LOCAL_IP_RANGE in self.client_address[0] or ((not self.headers.get("token") is None) and token==self.headers.get("token")):
            # if not self.headers.get("token") is None:
            #     print("'"+self.client_address[0]+"' successfully authenticated")
            # else:
            #     print("'"+self.client_address[0]+"' successfully authenticated locally")
            return True
        else:
            if not self.headers.get("token") is None:
                print("'"+self.client_address[0]+"' failed to authenticate with token")
            else:
                print("'"+self.client_address[0]+"' failed to authenticate without token")
            print("'"+token+"' needed to authenticate")
            return False

    def getToken(self):
        global HASH_SECRET
        m=hashlib.blake2b()
        m.update(self.client_address[0].encode())
        m.update(HASH_SECRET.encode())
        return m.hexdigest()

    def doPartial(self):
        global BUFFERSIZE
        filepath=self.translatePath()
        if filepath==None:
            self.respond("not found")
        try:
            range=parse_byte_range(self.headers.get("Range"))
        except ValueError as e:
            self.respond("range not satisfiable")
            return
        file=open(filepath,"rb")
        fs=os.fstat(file.fileno())
        file_len=fs[6]
        first,last=range

        if first is None:
            first=0

        if first >= file_len:
            self.respond("range not satisfiable")
            return
        self.setMime()
        self.setHeader('Accept-Ranges', 'bytes')

        if last is None or last >= file_len:
            last = min(file_len - 1,first+BUFFERSIZE-1)
        response_length = last - first + 1

        self.setHeader('Content-Range',
                         'bytes %s-%s/%s' % (first, last, file_len))
        self.setHeader('Content-Length', str(response_length))
        self.respond("partial content")
        file.seek(first)
        while 1:
            to_read = min(BUFFERSIZE, last + 1 - file.tell() if last else BUFFERSIZE)
            buf = file.read(to_read)
            if not buf:
                break
            self.handleWrite(buf)
        file.close()
        return

    def render(self,filestr,variables):
        global RENDER_VARIABLE_PREFIX, RENDER_VARIABLE_SUFFIX
        #variables = {something}
        #just replace the
        for key in variables.keys():
            filestr=filestr.replace(RENDER_VARIABLE_PREFIX+key+RENDER_VARIABLE_SUFFIX,variables[key])
        return filestr

    def setMime(self):
        mimetype,encoding=mimetypes.guess_type(self.path)
        if mimetype==None:
            self.mime="application/octet-stream"
        else:
            self.mime=mimetype
        return

    def setHeader(self,header_or_field, value=None):
        if value==None:
            field,value=header_or_field
        else:
            field=header_or_field
        if not hasattr(self, 'response_headers'):
            self.response_headers=[]
        else:
            self.response_headers.append((field,value))
        return

    def respond(self,mode="ok"):
        if not hasattr(self, 'response_headers'):
            self.response_headers=[]
        if mode.lower() in ["file","content","ok","true","200"]:
            self.handleStatus(200)
            self.handleHeader("Content-type", self.mime)
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
        elif mode.lower() in ["206","partial","partial content","partial ok","ok partial"]:
            self.handleStatus(206)
            self.handleHeader("Content-type", self.mime)
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
        elif mode.lower() in ["move","location","303","redirect"]:
            #location must be specified in self.location
            if self.location:
                self.handleStatus(303)
                for item in self.response_headers:
                    field,value=item
                    self.handleHeader(field,value)
                self.handleHeader("Location",self.location)
                self.handleHeaderEnd()
            else:
                return False
        elif mode.lower() in ["400","invalid range","invalid byte range","bad request","wrong","bad"]:
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleStatus(400)
            self.handleHeaderEnd()
            content=self.renderFile(LOCAL_PATH_400)
            if content==None:
                return
            else:
                self.handleWrite(content)
        elif mode.lower() in ["403","forbidden","token error"]:
            self.handleStatus(403)
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
            content=self.renderFile(LOCAL_PATH_403)
            if content==None:
                return
            else:
                self.handleWrite(content)
        elif mode.lower() in ["404","not found","missing"]:
            self.handleStatus(404) #because we just want to show the 404 page
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeader("Location","/404")
            self.handleHeaderEnd()
            content=self.renderFile(LOCAL_PATH_404)
            if content==None:
                return
            else:
                self.handleWrite(content)
        elif mode.lower() in ["409","conflict","exists","file exists"]:
            self.handleStatus(409)
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
            #most likely for put-requests, so maybe return json?
            content=self.renderFile(LOCAL_PATH_409)
            if content==None:
                return
            else:
                self.handleWrite(content)
        elif mode.lower() in ["416","range not satisfiable","not satisfiable"]:
            self.handleStatus(416)
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
            content=self.renderFile(LOCAL_PATH_416)
            if content==None:
                return
            else:
                self.handleWrite(content)
        elif mode.lower() in ["423","locked","ressource locked","content locked"]:
            self.handleStatus(423)
            self.handleHeader("Content-type", self.mime)
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
            content=self.renderFile(LOCAL_PATH_423)
            if content==None:
                return
            else:
                self.handleWrite(content)
        elif mode.lower() in ["504","unavailable","temporarily unavailable","content unavailable"]:
            self.handleStatus(504)
            self.handleHeader("Content-type", "text/html")
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
            content="server ensure running, request delayed.".encode()
            self.handleWrite(content)
        elif mode.lower() in ["random","irrelevant","abort","desinform"]:
            resp=200
            while resp==200:
                resp=random.randrange(1,1000)
            self.handleStatus(resp)
            for item in self.response_headers:
                field,value=item
                self.handleHeader(field,value)
            self.handleHeaderEnd()
        return True

    def getPostData(self):
        # Doesn't do anything with posted data
        if self.headers.get('Content-Length'):
            content_length = int(self.headers.get('Content-Length')) # <--- Gets the size of data
            # post_data = self.rfile.read(content_length) # <--- Gets the data itself
            # print(post_data)
            # return post_data

            ctype, pdict = cgi.parse_header(self.headers.get("Content-Type"))
            if pdict.get("boundary"):
                pdict["boundary"] = bytes(pdict.get("boundary"), "latin-1") #maybe change encoding later
            if ctype == "multipart/form-data":
                postvars = cgi.parse_multipart(self.rfile, pdict)
            elif ctype == 'application/x-www-form-urlencoded':
                postvars = urllib.parse.parse_qs(self.rfile.read(content_length), keep_blank_values=1)
            elif ctype == 'application/json':
                postvars = json.loads(self.rfile.read(content_length))
            else:
                postvars = None

            return postvars
        else:
            return None

    def sendContent(self, binContent):
        self.setHeader("Content-Length",sys.getsizeof(binContent))
        self.respond("ok")
        self.handleWrite(binContent)
        return

    def sendString(self, strContent):
        global LOCAL_PATH_INDEX
        self.path=LOCAL_PATH_INDEX
        self.setHeader("Content-Length",sys.getsizeof(strContent.encode()))
        self.setMime() #just handle as html string
        self.respond("ok")
        self.handleWrite(strContent.encode())
        return

    def sendJSON(self, status, data):
        self.path="/"
        data=json.dumps(data).encode()
        self.setHeader("Content-Length",sys.getsizeof(data))
        self.setMime()
        self.respond(str(status))
        self.handleWrite(data)

    def sendRequestedFile(self,variables={}):
        localpath=self.translatePath()
        if localpath!=None:
            if os.path.isfile(localpath):
                file=open(localpath,"r")
                self.setMime()
                strContent=file.read()
                self.sendContent(strContent.encode())
                file.close()
            else:
                self.respond("404")
        else:
            self.respond("404")

    def sendRequestedBinaryFile(self):
        localpath=self.translatePath()
        if localpath!=None:
            if os.path.isfile(localpath):
                file=open(localpath,"rb")
                self.setMime()
                content=file.read()
                self.sendContent(content)
                file.close()
            else:
                self.respond("404")
        else:
            self.respond("404")

    def sendTextFiles(self,files,variables={}):
        totalsize=0
        for file in files:
            if not os.path.isfile(file):
                self.respond("404")
                rendered+=renderFile(file,variables)
                return
        self.setHeader("Content-Length",str(sys.getsizeof(rendered)))
        self.setMime()
        self.respond("200")
        self.handleWrite(rendered.encode())
        return

    def sendRequestedFilePartial(self):
        localpath=self.translatePath()
        if localpath!=None:
            if os.path.isfile(localpath):
                self.doPartial()
            else:
                self.respond("404")
        else:
            self.respond("404")

    def renderFile(self,path,variables={}):
        if os.path.isfile(path):
            file=open(path,"r")
            content=file.read()
            file.close()
            return self.render(content,variables).encode()
        else:
            return None

    ###############################
    # Custom Functions            #
    ###############################

    def translatePath(self):
        return handlerextension.translatePath(self)

    def isSuspected(self):
        return handlerextension.isSuspected(self)

    def handleSuspicious(self):
        return handlerextension.handleSuspicious(self)

    def do_GET(self):
        return handlerextension.do_GET(self)

    def do_POST(self):
        return handlerextension.do_POST(self)

    def do_PUT(self):
        return handlerextension.do_PUT(self)

    def do_HEAD(self):
        return handlerextension.do_HEAD(self)

    def do_LOCK(self):
        return handlerextension.do_LOCK(self)

    def log_message(self,format,*args):
        return handlerextension.log_message(self,format,*args)

    def log_error(self,format,*args):
        return handlerextension.log_error(self,format,*args)



class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def wrapSSL(serversocket):
    global SSH_KEYFILE, SSH_CERTFILE
    if SSH_KEYFILE is None or SSH_CERTFILE is None or SSH_CERTFILE=="" or SSH_KEYFILE=="":
        return serversocket
    else:
        serversocket=ssl.wrap(serversocket, keyfile=SSH_KEYFILE, certfile=SSH_CERTFILE, serverside=True)
        return serversocket

def config():
    global LOCAL_PATH_400, LOCAL_PATH_403, LOCAL_PATH_404, LOCAL_PATH_409, LOCAL_PATH_416, LOCAL_PATH_423, LOCAL_PATH_INDEX, ROOT_DIR
    if not os.path.isfile("config.json") and not os.path.isfile("default_config.json"):
        print("could not find default_config.json! Please Clone the Repository again!")
        return False
    elif not os.path.isfile("config.json"):
        shutil.copyfile("default_config.json","config.json")

    configfile=open("config.json","r")
    config=json.load(configfile)

    # server os paths to basic server files
    LOCAL_PATH_INDEX=config["LOCAL_PATH_INDEX"]
    LOCAL_PATH_400=config["LOCAL_PATH_400"]
    LOCAL_PATH_403=config["LOCAL_PATH_403"]
    LOCAL_PATH_404=config["LOCAL_PATH_404"]
    LOCAL_PATH_409=config["LOCAL_PATH_409"]
    LOCAL_PATH_416=config["LOCAL_PATH_416"]
    LOCAL_PATH_423=config["LOCAL_PATH_423"]

    global LOCAL_PATH_LAYOUT
    LOCAL_PATH_LAYOUT=config["LOCAL_PATH_LAYOUT"]

    global LOCAL_PATH_COMMAND_PING
    LOCAL_PATH_COMMAND_PING=config["LOCAL_PATH_COMMAND_PING"]

    global RELATIVE_PATH_INDEX
    RELATIVE_PATH_INDEX=config["RELATIVE_PATH_INDEX"]

    global ROOT_DIR
    ROOT_DIR=config["ROOT_DIR"]

    global FILE_EXTENSIONS
    FILE_EXTENSIONS=config["FILE_EXTENSIONS"]


    global RENDER_VARIABLE_PREFIX, RENDER_VARIABLE_SUFFIX, BUFFERSIZE,CHUNKLENGTH
    # some more random variables
    RENDER_VARIABLE_PREFIX=config["RENDER_VARIABLE_PREFIX"]
    RENDER_VARIABLE_SUFFIX=config["RENDER_VARIABLE_SUFFIX"]
    BUFFERSIZE=config["BUFFERSIZE"]
    CHUNKLENGTH=BUFFERSIZE

    global SUSPICIOUS_ROUTES, SUSPICION_LEVEL_IPS
    #suspicion and ban system setup
    SUSPICIOUS_ROUTES=config["SUSPICIOUS_ROUTES"]
    SUSPICION_LEVEL_IPS={}

    global SSH_KEYFILE, SSH_CERTFILE, HASH_SECRET
    #security setup
    SSH_KEYFILE=config["SSH_KEYFILE"]
    SSH_CERTFILE=config["SSH_CERTFILE"]

    HASH_SECRET=config["HASH_SECRET"]

    global IP, PORT, LOCAL_IP_RANGE, OUTBOUND_IP
    IP=config["IP"]
    PORT=config["PORT"]
    LOCAL_IP_RANGE=config["LOCAL_IP_RANGE"]
    OUTBOUND_IP=config["OUTBOUND_IP"]

    global ENSURE_THRESHOLD
    ENSURE_THRESHOLD=config["ENSURE_THRESHOLD"]
    if ENSURE_THRESHOLD<=1:
        ENSURE_THRESHOLD=1

    global DB_URI, DB_NAME
    DB_URI=config["DB_URI"]
    DB_NAME=config["DB_NAME"]

    return True

if __name__ == '__main__':
    global IP,PORT,ROOT_DIR,DB_NAME,DB_URI
    success=config()
    if success:
        server = ThreadedHTTPServer((IP, PORT), Handler)
        server.socket=wrapSSL(server.socket)

        handlerextension.setGlobal("DB_NAME",DB_NAME)
        handlerextension.setGlobal("DB_URI",DB_URI)
        handlerextension.init()

        print('Starting server on IP: \''+IP+'\' PORT: '+str(PORT)+"\n")
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            print("Server Stopped.")
        except Exception as e:
            print("Error encountered: %s", e)
    else:
        print("could not load config. check errors.")
