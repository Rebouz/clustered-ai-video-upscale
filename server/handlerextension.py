import mimetypes, os, hashlib, calendar, time, math, json, threading, pymongo, bson
import urllib, zipfile, hashlib, sys
import cv2 as cv
import autoconvert, status

def init():
    global CLUSTERSIZE, CLUSTERTIMEOUT, ENSURE_RUNNING, ENSURE_SRC, ENSURE_VIDID, VIDINFOS, SOURCEFILES, WORKLOADS, DB_URI, DB_NAME
    db_client=pymongo.MongoClient(DB_URI)
    db=db_client[DB_NAME]
    SOURCEFILES=db.sourcefiles
    WORKLOADS=db.workloads
    VIDINFOS=db.properties
    ENSURE_RUNNING=False
    ENSURE_SRC=""
    ENSURE_VIDID=""
    vids=[]
    if WORKLOADS.count()==0:
        if SOURCEFILES.count()==0:
            print("need files to work on! Sourcefiles database collection is empty.")
            sys.exit()
        else:
            for item in SOURCEFILES.find({}):
                if os.path.isfile(item["name"]):
                    WORKLOADS.insert_one({"name": item["name"], "model": item["model"], "clusters":[],"status":"shallow"})
    else:
        forceTimeoutClusters()

    if not os.path.isfile("convertconfig.json"):
        with open("default_convertconfig.json","rb") as e:
            with open("convertconfig.json", "wb") as f:
                f.write(e.read())
    with open("convertconfig.json","r") as f:
        data=json.load(f)
    for k in data.keys():
        globals()[k]=data[k]
    for item in vids:
        getProperties(item["name"])
    autoconvert.init()


def setGlobal(name,value):
    globals()[name]=value
    return

def getGlobal(name):
    return globals()[name]

def getMaxClusters(name):
    global MAXCLUSTERS
    if not "MAXCLUSTERS" in globals():
        MAXCLUSTERS={}
    if not name in MAXCLUSTERS.keys():
        framecount,fps,height,width=getProperties(name)
        MAXCLUSTERS[name] = math.ceil(framecount/getGlobal("CLUSTERSIZE"))
        return math.ceil(framecount/getGlobal("CLUSTERSIZE"))
    else:
        return MAXCLUSTERS[name]

def createSrcForVideo(vidpath,fps,model):
    autoconvert.splitfile(videopath,fps,model)

def timeoutClusters():
    #removes all clusters that are to be timing out
    global CLUSTERTIMEOUT
    for vid in getGlobal("WORKLOADS").find({}):
        newclusters=[]
        for cluster in vid["clusters"]:
            if not (int(cluster["timestamp"])<calendar.timegm(time.gmtime())-CLUSTERTIMEOUT) or cluster["status"]=="finished":
                newclusters.append(cluster)
        WORKLOADS.find_one_and_update({"_id":vid["_id"]}, {"$set":{"clusters":newclusters}})

def forceTimeoutClusters():
    #removes all pending clusters
    for vid in getGlobal("WORKLOADS").find({}):
        newclusters=[]
        for cluster in vid["clusters"]:
            if cluster["status"]=="finished":
                newclusters.append(cluster)
        WORKLOADS.find_one_and_update({"_id":vid["_id"]}, {"$set":{"clusters":newclusters}})

def hasClusters(address,id):
    #checks if a worker has clusters still pending
    for item in getGlobal("WORKLOADS").find({}):
        for cluster in item["clusters"]:
            if cluster["ip"]==address and cluster["status"]=="pending" and cluster["id"]==id:
                return True
    else:
        return False

def isActiveID(id):
    #checks if given id is already being actively used
    for item in getGlobal("WORKLOADS").find({}):
        for cluster in item["clusters"]:
            if cluster["status"]=="pending" and cluster["id"]==id:
                return True
    else:
        return False

def getProperties(videostr):
    #gets essential videoproperties

    doc=getGlobal("VIDINFOS").find_one({"videostr":videostr})
    if doc!=None:
        return(doc["framecount"],doc["fps"],doc["width"],doc["height"])
    else:
        video=cv.VideoCapture(videostr)
        height=video.get(cv.CAP_PROP_FRAME_HEIGHT)
        width=video.get(cv.CAP_PROP_FRAME_WIDTH)
        fps=video.get(cv.CAP_PROP_FPS)
        framecount=int(video.get(cv.CAP_PROP_FRAME_COUNT))
        getGlobal("VIDINFOS").insert_one({"videostr":videostr,"framecount":framecount,"fps":fps,"width":width,"height":height})
    return (framecount,fps,width,height)

def clustersHasIndex(i,clusters):
    #checks if "clusters" includes a cluster with the specified index
    for cluster in clusters:
        if cluster["index"]==i:
            return True
    return False

def getNewCluster(ip,id):
    #generates a new cluster for the worker to work on
    global CLUSTERSIZE
    data=getGlobal("WORKLOADS").find({})
    for vid in data:
        framecount,fps,height,width=getProperties(vid["name"])
        if len(vid["clusters"])<=math.ceil(framecount/CLUSTERSIZE):
            i=0
            while i<math.ceil(framecount/CLUSTERSIZE): #fix error by one through doing exclusive lower than
                if not clustersHasIndex(i,vid["clusters"]):
                    shortname=vid["name"].split(os.sep)[-1].split(".")[0]
                    vid["clusters"].append({"id":id,"index":i, "ip":ip,"timestamp":calendar.timegm(time.gmtime()), "status":"pending"})
                    WORKLOADS.find_one_and_update({"_id":vid["_id"]},{"$set":{"clusters":vid["clusters"]}})
                    if vid["status"]=="ready":
                        WORKLOADS.find_one_and_update({"_id":vid["_id"]},{"$set":{"status":"pending"}})
                        print("'"+vid["name"]+"' is now pending.")
                    print("New Cluster for "+ip+" - Name: "+shortname+"' Index: '"+str(vid["clusters"][-1]["index"])+"'")
                    return (vid["name"],vid["model"],{"id":id,"index":i, "ip":ip,"timestamp":calendar.timegm(time.gmtime()), "status":"pending"})
                i+=1
    return None

def refreshClusterAssignment(ip,id):
    if not hasClusters(ip,id):
        return
    else:
        for vid in getGlobal("WORKLOADS").find({}):
            for cluster in vid["clusters"]:
                if cluster["ip"]==ip and cluster["id"]==id and cluster["status"]=="pending":
                    cluster["timestamp"]=calendar.timegm(time.gmtime())
                    WORKLOADS.find_one_and_update({"_id":vid["_id"]},{"$set":{"clusters":vid["clusters"]}})
                    return

def getNextCluster(ip,id):
    #returns the active or a new cluster for a worker
    timeoutClusters()
    if not hasClusters(ip,id):
        return getNewCluster(ip,id)
    else:
        for vid in getGlobal("WORKLOADS").find({}):
            for cluster in vid["clusters"]:
                if cluster["ip"]==ip and cluster["id"]==id and cluster["status"]=="pending":
                    return (vid["name"],vid["model"],cluster)

def getVidOfName(videoname):
    #returns the active or a new cluster for a worker
    for vid in getGlobal("WORKLOADS").find({}):
        shortvidname=vid["name"].split(os.sep)[-1]
        noext=shortvidname.split(".")[0]
        if videoname in [vid["name"],shortvidname,noext]:
            return vid
    return None

def makePictures(name,model,index):
    #generates a full cluster of image names for a specified video and index
    index=int(index)
    pics=[]
    framecount,fps,height,width=getProperties(name)
    for i in range(index*CLUSTERSIZE+1,min((index+1)*CLUSTERSIZE+1,framecount+1),1):
        pics.append(name.split(os.sep)[-1].replace(".mp4","^"+model+" "+str(i).zfill(6)+".png"))
    return pics

def ensureSources(name,model,framecount):
    #ensures a specific source to be ready to be served to the clients
    global ENSURE_RUNNING, ENSURE_SRC, ENSURE_VIDID
    shortname=name.split(os.sep)[-1].split(".")[0]
    projectpath=autoconvert.getProjectpath()
    autoconvert.createdirs(name,model)
    if not ENSURE_RUNNING:
        isEnsured=True
        for i in range(0,math.ceil(framecount/CLUSTERSIZE)):
            if not os.path.isfile(projectpath+os.sep+"in"+os.sep+shortname+"^"+model+os.sep+str(i)+".zip"):
                isEnsured=False
                break
        if getGlobal("WORKLOADS").find_one({"name":name})["status"]=="shallow":
            if not os.path.isfile(name):
                getGlobal("WORKLOADS").find_one_and_update({"name":name}, {"$set": {"status": "broken"}})
            else:
                getGlobal("WORKLOADS").find_one_and_update({"name":name}, {"$set": {"status": "splitting"}})
                print("Splitting '"+shortname+"'...")
                ENSURE_RUNNING=True
                ENSURE_SRC=shortname
                ENSURE_VIDID=str(getGlobal("WORKLOADS").find_one({"name":name})["_id"])
                framecount,fps,height,width=getProperties(name)
                infiles=os.listdir(projectpath+os.sep+"in"+os.sep+shortname+"^"+model)
                if len(infiles)<framecount:
                    for file in infiles:
                        os.remove(projectpath+os.sep+"in"+os.sep+shortname+"^"+model+os.sep+file)
                    autoconvert.splitfile(name,fps,model)
                getGlobal("WORKLOADS").find_one_and_update({"name":name}, {"$set": {"status": "compressing"}})
                print("Compressing '"+shortname+"'...")
                for index in range(0,math.ceil(framecount/CLUSTERSIZE)):
                    pics=makePictures(name,model,index)
                    with zipfile.ZipFile(projectpath+os.sep+"in"+os.sep+shortname+"^"+model+os.sep+str(index)+".zip","w") as zip:
                        for file in pics:
                            zip.write(projectpath+os.sep+"in"+os.sep+shortname+"^"+model+os.sep+file,file)
                print("Finished Ensuring '"+shortname+"', file is now ready.")
                getGlobal("WORKLOADS").find_one_and_update({"name":name}, {"$set": {"status": "ready"}})
                ENSURE_RUNNING=False
                ENSURE_SRC=""
                ENSURE_VIDID=""

def ensureData(ensurethreshold):
    if ensurethreshold<1:
        ensurethreshold=1
    #ensures sources that are to be processed
    #in the near future are ready.
    #this means current+1
    projectpath=autoconvert.getProjectpath()
    workloads=getGlobal("WORKLOADS").find({})
    i=-1
    ensurenext=False
    for vid in workloads:
        if (vid["clusters"]==[] or vid["status"]=="shallow") and vid["status"]!="broken":
            ensurenext=True
        if ensurenext:
            i+=1
            framecount,fps,width,height=getProperties(vid["name"])
            t=threading.Thread(target=ensureSources, args=(vid["name"],vid["model"],framecount,), daemon=True)
            t.start()
        if i>=ensurethreshold:
            return

def rejoinVideos():
    #rejoin videos that are completed
    for vid in getGlobal("WORKLOADS").find({"clusters": {"$ne":[]}, "status": "pending"}):
        framecount,fps,height,width=getProperties(vid["name"])
        if math.ceil(framecount/CLUSTERSIZE)==len(vid["clusters"]):
            finished=True
            for cluster in vid["clusters"]:
                if cluster["status"]=="pending":
                    finished=False
                    break
            outfile=("content"+os.sep+"out"+os.sep+vid["name"].split(os.sep)[-1]).rsplit(".",1)[0]+"."+vid["model"]+"."
            outfile+=("content"+os.sep+"out"+os.sep+vid["name"].split(os.sep)[-1]).rsplit(".",1)[-1]
            if finished and not os.path.isfile(outfile):
                t=threading.Thread(target=rejoinControl, args=(vid["name"],vid["model"],fps,str(vid["_id"]),), daemon=True)
                t.start()

def rejoinControl(name, model, fps, wid):
    getGlobal("WORKLOADS").find_one_and_update({"_id": bson.objectid.ObjectId(wid)},{"$set": {"status": "rejoining"}})
    print("rejoining frames of '"+name+"'...")
    autoconvert.rejoin(name, model, fps)
    getGlobal("WORKLOADS").find_one_and_update({"_id": bson.objectid.ObjectId(wid)},{"$set": {"status": "finished"}})
    print("'"+name+"' has been finished.")
    return


def finish(ip,id):
    # print("try finishing up ip:",ip,"id:",id)
    isfinished=False
    data=getGlobal("WORKLOADS").find({"status":"pending"})
    info={}
    for item in data:
        for cluster in item["clusters"]:
            if cluster["ip"]==ip and cluster["status"]=="pending" and cluster["id"]==id:
                info={"name":item["name"],"model":item["model"],"cluster":cluster}
                isfinished=finalize(info)
                if isfinished:
                    print("'"+item["name"]+"' Cluster "+str(cluster["index"])+" from '"+ip+"', ID: '"+id+"' has been validated.")
                    cluster["status"]="finished"
                    t=threading.Thread(target=rejoinVideos, daemon=True)
                    t.start()
                    getGlobal("WORKLOADS").find_one_and_update({"_id": item["_id"]},{"$set": {"clusters": item["clusters"]}})
                    return isfinished
                else:
                    print("'"+item["name"]+"' Cluster "+str(cluster["index"])+" from '"+ip+"', ID: '"+id+"' is not valid.")
    return isfinished

def finalize(info):
    basename=info["name"].split(os.sep)[-1].split(".")[0]
    if os.path.isfile("."+os.sep+"content"+os.sep+"out"+os.sep+basename+"^"+info["model"]+os.sep+str(info["cluster"]["index"])+".zip"):
        with zipfile.ZipFile("content"+os.sep+"out"+os.sep+basename+"^"+info["model"]+os.sep+str(info["cluster"]["index"])+".zip", "r") as zip:
            zip.extractall("content"+os.sep+"out"+os.sep+basename+"^"+info["model"]+os.sep)
        os.remove("content"+os.sep+"out"+os.sep+basename+"^"+info["model"]+os.sep+str(info["cluster"]["index"])+".zip")
        pics=makePictures(info["name"],info["model"],info["cluster"]["index"])
        return True
    else:
        return False


def translatePath(self):
    ROOT_DIR, FILE_EXTENSIONS=self.getGlobal("ROOT_DIR"),self.getGlobal("FILE_EXTENSIONS")
    if "/.." in self.path:
        return None
    else:
        filepath = ROOT_DIR+self.path.replace("/",os.sep)
        if os.path.isfile(filepath):
            return filepath
        else:
            for ext in FILE_EXTENSIONS:
                filepath2 = filepath+ext
                if os.path.isfile(filepath2):
                    return filepath2
            return None

def untranslatePath(self):
    ROOT_DIR, FILE_EXTENSIONS=self.getGlobal("ROOT_DIR"),self.getGlobal("FILE_EXTENSIONS")
    if os.sep+".." in self.path:
        return None
    else:
        filepath = self.path.replace("ROOT_DIR","\\").replace(os.sep,"/")
        return None

def isSuspected(self):
    SUSPICIOUS_ROUTES, SUSPICION_LEVEL_IPS = self.getGlobal("SUSPICIOUS_ROUTES"),self.getGlobal("SUSPICION_LEVEL_IPS")
    if self.client_address[0] in SUSPICION_LEVEL_IPS.keys() and SUSPICION_LEVEL_IPS[client_address[0]]>=5:
        #do some AbuseIPDB banning or similar.
        return True
    elif self.path in SUSPICIOUS_ROUTES:
        return True
    return False

def handleSuspicious(self):
    SUSPICION_LEVEL_IPS=self.getGlobal("SUSPICION_LEVEL_IPS")
    if self.isSuspected():
        #do suspected stuff
        if self.client_address[0] in SUSPICIOUS_LEVEL_IPS.keys():
            SUSPICION_LEVEL_IPS[self.client_address[0]]+=1
        else:
            SUSPICION_LEVEL_IPS[self.client_address[0]]=1
        self.respond("random")
        self.setGlobal("SUSPICION_LEVEL_IPS",SUSPICION_LEVEL_IPS)
        return True
    self.setGlobal("SUSPICION_LEVEL_IPS",SUSPICION_LEVEL_IPS)
    return False

def do_GET(self):
    self.path=urllib.parse.unquote(self.path)
    LOCAL_PATH_COMMAND_PING=self.getGlobal("LOCAL_PATH_COMMAND_PING")
    ROOT_DIR=self.getGlobal("ROOT_DIR")
    LOCAL_PATH_LAYOUT=self.getGlobal("LOCAL_PATH_LAYOUT")
    RELATIVE_PATH_INDEX=self.getGlobal("RELATIVE_PATH_INDEX")
    ENSURE_THRESHOLD=self.getGlobal("ENSURE_THRESHOLD")
    ENSURE_SRC=getGlobal("ENSURE_SRC")
    ENSURE_VIDID=getGlobal("ENSURE_VIDID")
    ENSURE_RUNNING=getGlobal("ENSURE_RUNNING")
    if self.handleSuspicious():
        #we responded to a malicious request.
        #better not process any further,
        #cpu-time is more important.
        return

    #is this a command? do some special stuff then.
    if self.path=="/ping":
        self.sendTextFiles([LOCAL_PATH_COMMAND_PING],{"headers":str(self.headers).replace("\n","<br />")})
        return

    elif self.path.split("/")[1] in ["status", "status_list", "statuslist", "status"]:
        if len(self.path.split("/"))==2:
            self.reqpath=self.path
            self.path="/overview_status.html" #make mimetype html
            self.setMime()
            self.respond("ok")
            content=self.renderFile("partials"+os.sep+"layout.html")+self.renderFile("partials"+os.sep+"overview_status.html")
            self.handleWrite(content)
        elif len(self.path.split("/"))==3:
            vidid=self.path.split("/")[2]
            self.reqpath=self.path
            self.path="/status.html" #make mimetype html
            self.setMime()
            self.respond("ok")
            content=self.renderFile("partials"+os.sep+"layout.html")+self.renderFile("partials"+os.sep+"status.html",{"vidid":vidid})
            self.handleWrite(content)
        return

    elif len(self.path.split("/"))>1 and self.path.split("/")[1]=="getworkload":
        if self.isAuthenticated():
            if len(self.path.split("/"))==2:
                id="0"
            else:
                id=self.path.split("/")[-1]
            name,model,cluster=getNextCluster(self.client_address[0],id)
            framecount,fps,width,height=getProperties(name)
            ensureData(ENSURE_THRESHOLD)
            t=threading.Thread(target=rejoinVideos, daemon=True)
            t.start()
            #makes sure theres always all pending videos + 1
            #ready to be served to workers
            content=name.split("\\")[-1].split("/")[-1].split(os.sep)[-1].split(".")[0]+"\n"
            content+=model+"\n"
            content+=str(height)+"\n"
            content+=str(width)+"\n"
            content+=str(framecount)+"\n"
            content+=str(cluster["index"])+"\n"
            for item in makePictures(name,model,cluster["index"]):
                content+=item+"\n"
            self.sendString(content)
        else:
            self.respond("random")
        return

    elif self.path=="/getenv":
        self.path="/setup_env"
        self.setMime()
        if self.getGlobal("LOCAL_IP_RANGE") in self.client_address[0]:
            self.sendContent(self.renderFile("setup_env.py",{"ip":self.getGlobal("IP"),"port":str(self.getGlobal("PORT"))}))
        else:
            self.sendContent(self.renderFile("setup_env.py",{"ip":self.getGlobal("OUTBOUND_IP"),"port":str(self.getGlobal("PORT"))}))
        return

    elif self.path=="/getworkloadhandler":
        self.path="/start_workload"
        self.setMime()
        if self.getGlobal("LOCAL_IP_RANGE") in self.client_address[0]:
            self.sendContent(self.renderFile("start_workload.py",{"ip":self.getGlobal("IP"),"port":str(self.getGlobal("PORT"))}))
        else:
            self.sendContent(self.renderFile("start_workload.py",{"ip":self.getGlobal("OUTBOUND_IP"),"port":str(self.getGlobal("PORT"))}))
        return

    elif self.path=="/getautoconvert":
        self.path="/autoconvert"
        self.setMime()
        if self.getGlobal("LOCAL_IP_RANGE") in self.client_address[0]:
            self.sendContent(self.renderFile("autoconvert.py",{"ip":self.getGlobal("IP"),"port":str(self.getGlobal("PORT"))}))
        else:
            self.sendContent(self.renderFile("autoconvert.py",{"ip":self.getGlobal("OUTBOUND_IP"),"port":str(self.getGlobal("PORT"))}))
        return

    elif self.path=="/getworkingprogram":
        self.path="/working_program.zip"
        self.setMime()
        with open("content"+os.sep+"working_program.zip","rb") as f:
            self.sendContent(f.read())
        return

    elif self.path=="/getconvertconfig":
        self.path="/convertconfig.json"
        self.setMime()
        with open("convertconfig.json","rb") as f:
            self.sendContent(f.read())
        return

    elif self.path=="/getworkerconfig":
        self.path="/default_worker_config.json"
        self.setMime()
        with open("default_worker_config.json","rb") as f:
            self.sendContent(f.read())

    elif self.path.split("/")[1]=="finish_workload":
        if self.isAuthenticated():
            if len(self.path.split("/"))==2:
                id="0"
            else:
                id=self.path.split("/")[-1]
            if (ENSURE_RUNNING):
                refreshClusterAssignment(self.client_address[0],id)
                self.respond("504")
                return
            isfinished=finish(self.client_address[0],id)
            if isfinished:
                self.sendString("y")
            else:
                self.sendString("n")
        else:
            self.respond("random")
        return

    elif self.path.split("/")[1]=="isid":
        if len(self.path.split("/"))==2:
            id="0"
        else:
            id=self.path.split("/")[2]
        if isActiveID(id):
            self.sendString("y")
        else:
            self.sendString("n")
        return

    elif self.path.split("/")[1]=="redo":
        forceTimeoutClusters()
        self.location="/status"
        self.respond("redirect")
        return

    elif self.path.split("/")[1] in ["ensure","ensuredata","prep","prepare","ensureAll","prepareAll"]:
        timeoutClusters()
        ensureData(ENSURE_THRESHOLD)
        self.location="/status"
        self.respond("redirect")
        return

    elif self.path.split("/")[1] in ["join","con","rejoin","concat","makevid","tovid","fin","finish"]:
        timeoutClusters()
        ensureData(ENSURE_THRESHOLD)
        t=threading.Thread(target=rejoinVideos, daemon=True)
        t.start()
        self.location="/status"
        self.respond("redirect")
        return

    elif self.path.split("/")[1]=="gettoken":
        self.sendString(self.getToken())
        return

    elif self.path in ["/client","/client.py","/getclient","/localclient","/getlocalclient","/localclient.py"]:
        self.path="/client"
        self.setMime()
        if self.getGlobal("LOCAL_IP_RANGE") in self.client_address[0]:
            self.sendContent(self.renderFile("content"+os.sep+"client.py",{"ip":self.getGlobal("IP"),"port":str(self.getGlobal("PORT"))}))
        else:
            self.sendContent(self.renderFile("content"+os.sep+"client.py",{"ip":self.getGlobal("OUTBOUND_IP"),"port":str(self.getGlobal("PORT"))}))
        return

    #can we guess a mimetype here?
    #if not - just append a possible extension
    if len(self.path.split("."))==1 and self.path[-1]!="/":
        self.path+=".html"
    elif len(self.path.split("."))==1:
        self.path+=RELATIVE_PATH_INDEX

    ENSURE_SRC=globals()["ENSURE_SRC"]
    if getGlobal("ENSURE_RUNNING") and self.path.split("/")[1:2] in [["out",ENSURE_SRC],["in",ENSURE_SRC]]:
        self.respond("504")
        return

    translatedPath = self.translatePath()
    if translatedPath != None:
        mimetype,encoding=mimetypes.guess_type(translatedPath)
        #make some assumption about when to do partial and when to send
        #complete file
        try:
            if mimetype.split("/")[0] in ["text"]:
                self.sendRequestedFile()
            elif mimetype.split("/")[0] in ["video"]:
                self.sendRequestedFilePartial()
            else:
                self.sendRequestedBinaryFile()
        except:
            self.respond("500")
    else:
        self.respond("404")
    return

def do_POST(self):
    if self.handleSuspicious():
        #we responded to a malicious request.
        #better not process any further,
        #cpu-time is more important.
        return

    postvars = self.getPostData()
    if (not postvars.get("token") is None) and postvars.get("token")[0].decode()==self.getToken():
        if ENSURE_RUNNING:
            id=postvars.get("id")[0]
            id=id.decode()
            refreshClusterAssignment(self.client_address[0],id)
            self.respond("504")
            return
        fname = postvars.get("filename")[0].decode()
        if fname[0] in [os.sep,"/"]:
            fname=fname[1:]
        # print("Posted file '", postvars.get("filename")[0].decode()+"'")
        if postvars.get("file") and postvars.get("filename"):
            file = postvars.get("file")[0]
            viddir="."+os.sep+"content"
            for folder in fname.split("/")[:-1]:
                viddir+=os.sep+folder
                if not os.path.isdir(viddir):
                    print("creating dir '"+viddir+"' for posted file")
                    os.mkdir(viddir)
            with open("content"+os.sep+fname.replace("/",os.sep), "wb") as fw:
                fw.write(file)
        if postvars != None:
            self.sendString("success!")
        else:
            self.sendString("failure!")
    else:
        if (not postvars.get("token") is None):
            print("'"+self.client_address[0]+"' used wrong Token.")
        else:
            #handle commands from within local network
            if self.getGlobal("LOCAL_IP_RANGE") in self.client_address[0] and postvars.get("action") != None:
                action=postvars.get("action")
                if action=="redo" and postvars.get("video")==None:
                    forceTimeoutClusters()
                    self.sendJSON({'message': 'forced cluster timeout on all clusters'})
                    return
                elif action=="redo":
                    obj=json.loads(postvars.get("obj"))
                    if obj["type"]=="video":
                        workloads=getGlobal("WORKLOADS").find({})
                        for w in workloads:
                            if w["name"]==obj["name"]:
                                w["clusters"]=[c for c in w["clusters"] if c["status"]!="pending"]
                                getGlobal("WORKLOADS").find_one_and_update({"_id": w["_id"]},{"$set": {"clusters": w["clusters"]}})

                        self.sendJSON({'message','successfully timed out clusters of "'+obj["name"]+'"'})
                        return
                    elif obj["type"]=="cluster":
                        for w in workloads:
                            if w["name"]==obj["name"]:
                                w["clusters"]=[c for c in w["clusters"] if not (c["status"]=="pending" and c["index"]==obj["index"])]
                                getGlobal("WORKLOADS").find_one_and_update({"_id": w["_id"]},{"$set": {"clusters": w["clusters"]}})
                                self.sendJSON({'message','successfully timed out Cluster '+str(obj["index"])+' of "'+obj["name"]+'"'})
                                return
                elif action in ["ensure","ensuredata","prep","prepare","ensureAll","prepareAll"]:
                    timeoutClusters()
                    ensureData(self.getGlobal("ENSURE_THRESHOLD"))
                    if globals()["ENSURE_RUNNING"]==True:
                        self.sendJSON(200, {'message':'ensure is already running. couldn\'t ensure further sources.'})
                    else:
                        self.sendJSON(200,{'message':'successfully started ensuring of data.'})
                    return
                elif action in ["join","con","rejoin","concat","makevid","tovid","fin","finish"]:
                    timeoutClusters()
                    ensureData(self.getGlobal("ENSURE_THRESHOLD"))
                    t=threading.Thread(target=rejoinVideos, daemon=True)
                    t.start()
                    self.sendJSON(200,{'message':'successfully started rejoin process.'})
                    return
                elif action in ["timeout","forcetimeout"]:
                    forceTimeoutClusters()
                    self.sendJSON(200,{'message':'force timed out all pending clusters.'})
                    return
                elif action in ["getworkload","workload","dummy","dummyworkload","assign"]:
                    name,model,cluster=getNextCluster(self.client_address[0],"000000")
                    if hasClusters(self.client_address[0],"000000"):
                        self.sendJSON(200,{'message':'could not assign dummy workload. It is already assigned.'})
                    else:
                        self.sendJSON(206,{'message':'created dummy workload for "'+name+'" / cluster "'+cluster+'"'})
                    return
                elif action in ["allData","getData","getVideos","fullStatus","status","status_list","statuslist"]:
                    data=status.getAllData()
                    if self.getGlobal("LOCAL_IP_RANGE") in self.client_address[0]:
                        data["general_info"]["islocal"]=True
                    else:
                        data["general_info"]["islocal"]=False
                    self.sendJSON(200,data)
                    return
                elif action in ["getVidData","vidData","videoData","getVideoData"]:
                    data=status.getVidData(getGlobal("WORKLOADS").find_one({"_id":bson.objectid.ObjectId(postvars.get("vidid"))}))
                    if self.getGlobal("LOCAL_IP_RANGE") in self.client_address[0]:
                        data["general_info"]["islocal"]=True
                    else:
                        data["general_info"]["islocal"]=False
                    self.sendJSON(200,data)
                    return
                else:
                    print("got unrecognized action")
                    self.sendJSON(400,{'message':'action not recognized.'})
                    return
            else:
                print("wrong authentication on post without token")
                self.respond("random")
                return
    return

def do_PUT(self):
    self.respond("random")
    return

def do_HEAD(self):
    self.respond("random")
    return

def do_LOCK(self):
    self.respond("random")
    return

def log_message(self,format,*args):
    # print(self.client_address[0]+": "+self.command+" '"+self.path+"'")
    return

def log_error(self,format,*args):
    if self.client_address[0]==None:
        self.client_address[0]="[none ip]"
    if self.command==None:
        self.command="[none command]"
    if not "path" in self or self.path==None:
        self.path="[none path]"
    print(self.client_address[0]+": Exception on "+self.command+" '"+self.path+"'")
    return
