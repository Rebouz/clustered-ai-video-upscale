import os,sys,requests,urllib,json,secrets,zipfile,time,hashlib,socket,shutil
import autoconvert

def removeTempFiles():
    if os.path.isfile("setup_env.py"):
        os.remove("setup_env.py")
    if os.path.isfile("working_program.zip"):
        os.remove("working_program.zip")
    if os.path.isdir("__pycache__"):
        shutil.rmtree("__pycache__")
    return

def authenticate():
    headers={"token":getToken()}
    return headers

def fetchToken():
    global TOKEN, TIMEOUT
    s_code=404
    while s_code!=200:
        try:
            r=requests.get("http://${ip}:${port}/gettoken/")
            #no timout to prevent stupid stuff
            s_code=r.status_code
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout):
            s_code=500
            pass
    TOKEN=r.text
    print("Got Token:",TOKEN)
    return TOKEN

def getToken():
    global TOKEN
    if not "TOKEN" in globals().keys():
        TOKEN=fetchToken()
    return TOKEN

def main():
    getConfig()
    removeTempFiles()
    getToken()
    while True:
        rawname,model,height,width,index,pics=getNext()
        convert(rawname,model,width,height,pics)
        returnWorkload(index,model,rawname)
        wrapUp(rawname)

def strToBool(str):
    return (str.lower() in ['true', '1', 't', 'y', 'yes', "+", "^"])


def getNext():
    global TIMEOUT
    print("getting new workload info...")
    s_code=404
    while s_code!=200:
        try:
            r=requests.get("http://${ip}:${port}/getworkload/"+globals()["ID"], headers=authenticate(), timeout=TIMEOUT)
            s_code=r.status_code
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout, requests.exceptions.ReadTimeout):
            s_code=500
            pass
    data=r.text.split("\n")
    print("got new workload:\n"+str(data[:6]))
    rawname=data[0]
    model=data[1]
    height=float(data[2])
    width=float(data[3])
    framecount=int(data[4])
    index=str(data[5])
    pics=data[6:-1]

    if not os.path.isdir("content"+os.sep+"in"+os.sep+rawname+"^"+model):
        os.mkdir("content"+os.sep+"in"+os.sep+rawname+"^"+model)

    if not os.path.isdir("content"+os.sep+"out"+os.sep+rawname+"^"+model):
        os.mkdir("content"+os.sep+"out"+os.sep+rawname+"^"+model)

    if not infilesInPlace(rawname,model,pics):
        print("clearing remaining in files...")
        clearIn(model,rawname)

        print("retrieving new files from workload",end="")
        s_code=404
        while s_code!=200:
            print(".",end="")
            try:
                r=requests.get("http://${ip}:${port}/"+urllib.parse.quote("in/"+rawname+"^"+model+"/"+index+".zip"), timeout=TIMEOUT)
                with open("content/in/"+rawname+"^"+model+"/"+index+".zip","wb") as f:
                    f.write(r.content)
                s_code=r.status_code
                if s_code!=200:
                    time.sleep(10)
            except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout, requests.exceptions.ReadTimeout):
                s_code=500
                pass
        print("")
        print("extracting files...")
        with zipfile.ZipFile("content/in/"+rawname+"^"+model+"/"+index+".zip", "r") as zip:
            zip.extractall("content/in/"+rawname+"^"+model+"/")
        os.remove("content/in/"+rawname+"^"+model+"/"+index+".zip")
    return (rawname,model,height,width,index,pics)

def infilesInPlace(rawname,model,pics):
    for pic in pics:
        if not os.path.isfile("."+os.sep+"content"+os.sep+"in"+os.sep+rawname+"^"+model+os.sep+pic):
            return False
    return True

def outfilesInPlace(rawname,model,pics):
    for pic in pics:
        if not os.path.isfile("."+os.sep+"content"+os.sep+"out"+os.sep+rawname+"^"+model+os.sep+pic):
            return False
    return True

def convert(rawname,model,width,height,pics):
    if not outfilesInPlace(rawname,model,pics):
        print("clearing remaining out files...")
        clearOut(model,rawname)
        print("scaling up '"+rawname+"' ...")
        autoconvert.scaleup(rawname,model,width,height)
    else:
        print("skipping scaleup, all pictures already processed.")

def returnWorkload(index,model,rawname):
    print("zipping files...")
    files=[]
    for item in os.listdir("content"+os.sep+"out"+os.sep+rawname+"^"+model):
        if item.split(".")[-1]=="png":
            files.append("content"+os.sep+"out"+os.sep+rawname+"^"+model+os.sep+item)
    with zipfile.ZipFile("content"+os.sep+"out"+os.sep+rawname+"^"+model+os.sep+index+".zip","w") as zip:
        for file in files:
            zip.write(file,os.path.basename(file))
    print("submitting files")
    s_code=404
    while s_code!=200:
        print(".",end="")
        try:
            file=open("content"+os.sep+"out"+os.sep+rawname+"^"+model+os.sep+index+".zip","rb")
            files={"file":file}
            id=globals()["ID"]
            payload={"filename":"/out/"+rawname+"^"+model+"/"+index+".zip", "token":getToken(), "id": id}
            r=requests.post("http://${ip}:${port}/", data=payload, files=files)
            s_code=r.status_code
            if s_code==504:
                print("\nserver under ensure Load, submit delayed.")
                time.sleep(globals()["ENSURE_DELAY"]) #wait 10 minutes, service delayed due to ensure running
            elif s_code!=200:
                time.sleep(10)
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout, requests.exceptions.ReadTimeout):
            s_code=500
            time.sleep(10)
            pass
    print("")
    return

def wrapUp(rawname):
    s_code=404
    headers=authenticate()
    response="n"
    while s_code!=200 and response!="y":
        try:
            r=requests.get("http://${ip}:${port}/finish_workload/"+globals()["ID"], headers=headers)
            s_code=r.status_code
            response=r.text
            if s_code==504:
                print("server under ensure load, wrap up delayed.")
                time.sleep(globals()["ENSURE_DELAY"]) #wait 10 minutes, service delayed due to ensure running
            elif s_code!=200:
                time.sleep(10)
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout, requests.exceptions.ReadTimeout):
            s_code=500
            pass
        print("failed wrapup with ID:",globals()["ID"],"Headers:",headers,"\nretrying...")
    print("\n\n",end="")
    return

def clear(model,rawname=None):
    clearIn(model,rawname)
    clearOut(model,rawname)

def clearIn(model,rawname=None):
    if not rawname is None:
        for item in os.listdir("content"+os.sep+"in"+os.sep+rawname+"^"+model):
            os.remove("content"+os.sep+"in"+os.sep+rawname+"^"+model+os.sep+item)
    else:
        for rawname in os.listdir("content"+os.sep+"in"):
            for item in os.listdir("content"+os.sep+"in"+os.sep+rawname):
                os.remove("content"+os.sep+"in"+os.sep+rawname+os.sep+item)

def clearOut(model,rawname=None):
    if not rawname is None:
        for item in os.listdir("content"+os.sep+"out"+os.sep+rawname+"^"+model):
            os.remove("content"+os.sep+"out"+os.sep+rawname+"^"+model+os.sep+item)
    else:
        for rawname in os.listdir("content"+os.sep+"out"):
            for item in os.listdir("content"+os.sep+"out"+os.sep+rawname):
                os.remove("content"+os.sep+"out"+os.sep+rawname+os.sep+item)

def getConfig():
    with open("worker_config.json","r") as f:
        data=json.load(f)

    if not "ID" in data.keys():
        globals()["ID"]=newID()
        data["ID"]=globals()["ID"]
        with open("worker_config.json","w") as f:
            json.dump(data,f)
    else:
        globals()["ID"]=data["ID"]

    if not "ENSURE_DELAY" in data.keys():
        globals()["ENSURE_DELAY"]=600
        data["ENSURE_DELAY"]=ENSURE_DELAY
        with open("worker_config.json","w") as f:
            json.dump(data,f)
    else:
        globals()["ENSURE_DELAY"]=data["ENSURE_DELAY"]

    if not "MAX_ID" in data.keys():
        globals()["MAX_ID"]=10000000
        data["MAX_ID"]=globals()["MAX_ID"]
        with open("worker_config.json","w") as f:
            json.dump(data,f)
    else:
        globals()["MAX_ID"]=data["MAX_ID"]

    if not "TIMEOUT" in data.keys():
        globals()["TIMEOUT"]=10
        data["TIMEOUT"]=globals()["TIMEOUT"]
        with open("worker_config.json","w") as f:
            json.dump(data,f)
    else:
        globals()["TIMEOUT"]=data["TIMEOUT"]

def newID():
    id=str(secrets.randbelow(10000000))
    print("Setting new ID to '"+id+"'")
    with open("worker_config.json","r") as f:
        data=json.load(f)
    data["ID"]=id
    with open("worker_config.json","w") as f:
        json.dump(data,f) #max 1000 parallel connections
    return id

def isActiveID(id):
    s_code=400
    while s_code!=200:
        try:
            r=requests.get("http://${ip}:${port}/isid/"+id, timeout=globals()["TIMEOUT"])
            s_code=r.status_code
            if s_code!=200:
                time.sleep(10)
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout, requests.exceptions.ReadTimeout):
            s_code=500
    return r.text

main()
