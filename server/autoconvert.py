import os,sys,subprocess,time,threading,json,datetime
from multiprocessing import Process

def showStatus(rawname,model):
    global UPDATEDELAY
    i=0
    while True:
        i+=1
        time.sleep(UPDATEDELAY)
        folder=rawname+"^"+model
        inlen=len(os.listdir("content"+os.sep+"in"+os.sep+folder))
        outlen=len(os.listdir("content"+os.sep+"out"+os.sep+folder))
        t=datetime.datetime.now()
        formatted_time="["+str(t.hour).zfill(2)+":"+str(t.minute).zfill(2)+":"+str(t.second).zfill(2)+"]"
        if i>=300:
            print(formatted_time+" Processed "+str(outlen)+"/"+str(inlen)+" images.")
            i=0
        if outlen >= inlen:
            print(formatted_time+" all images processed. terminating watchdog.")
            return

def init():
    global cuipath, CLUSTERSIZE
    if not "projectpath" in globals().keys():
        global projectpath
        projectpath=os.getcwd()+os.sep+"content"
    if not "ffmpegpath" in globals().keys() or "projectpath" in globals().keys(): #refresh path on projectpath update
        global ffmpegpath
        ffmpegpath=projectpath+os.sep+"ffmpeg-latest-win64-static"+os.sep+"bin"+os.sep+"ffmpeg.exe"
    if not "waifu2xpath" in globals().keys() or "projectpath" in globals().keys():
        global waifu2xpath
        waifu2xpath=projectpath+os.sep+"waifu2x-caffe"
    if not "cuipath" in globals().keys() or "waifu2xpath" in globals().keys():
        global cuipath
        cuipath=waifu2xpath+os.sep+"waifu2x-caffe-cui.exe"
    if not "bitrate" in globals().keys():
        global bitrate
        bitrate="20M" #Bitrate for Video Export
    with open("convertconfig.json") as f:
        data=json.load(f)
    for k in data.keys():
        globals()[k]=data[k]

def getProjectpath():
    init()
    global projectpath
    return projectpath

def createdirs(videostr,model):
    init()
    projectpath=getProjectpath()
    if not os.path.isdir(projectpath+os.sep+"in"):
        os.mkdir(projectpath+os.sep+"in")
    if not os.path.isdir(projectpath+os.sep+"out"):
        os.mkdir(projectpath+os.sep+"out")
    videostr=videostr.split(os.sep)[-1].split(".")[0]
    if not os.path.isdir(projectpath+os.sep+"in"+os.sep+videostr.split(os.sep)[-1].split(".")[0]+"^"+model):
        os.mkdir(projectpath+os.sep+"in"+os.sep+videostr.split(os.sep)[-1].split(".")[0]+"^"+model)

    if not os.path.isdir(projectpath+os.sep+"out"+os.sep+videostr.split(os.sep)[-1].split(".")[0]+"^"+model):
        os.mkdir(projectpath+os.sep+"out"+os.sep+videostr.split(os.sep)[-1].split(".")[0]+"^"+model)

def validate():
    init()
    global ffmpegpath,waifu2xpath,projectpath,cuipath,bitrate
    if not os.path.isdir(projectpath):
        print("Project Waifu path not found.")
        print(projectpath)
        sys.exit()
    elif not os.path.isfile(ffmpegpath):
        print("FFMPEG not found.")
        print(ffmpegpath)
        sys.exit()
    elif not os.path.isdir(waifu2xpath):
        print("waifu2x-caffe folder not found.")
        print(waifu2xpath)
        sys.exit()
    elif not os.path.isfile(cuipath):
        print("waifu2x cui file not found.")
        print(cuipath)
        sys.exit()

#Serverside
def splitfile(videostr,fps,model):
    init()
    global ffmpegpath,waifu2xpath,projectpath,cuipath,bitrate
    validate()

    infolder=projectpath+os.sep+"in"+os.sep+videostr.split(os.sep)[-1].split(".")[0]+"^"+model
    outfolder=projectpath+os.sep+"out"+os.sep+videostr.split(os.sep)[-1].split(".")[0]+"^"+model
    outstr=outfolder+".mp4"

    videoformats=["mpeg","webm","mp4","avi","flv","mov","mkv","wmv","mpg","3gp"]
    for item in videoformats:
        if videostr.split(".")[-1]==item:
            sequencestr_in=infolder+os.sep+videostr.split(os.sep)[-1].replace("."+item,"^"+model+" %06d.png")
            sequencestr_out=outfolder+os.sep+videostr.split(os.sep)[-1].replace("."+item,"^"+model+" %06d.png")
    if not " %06d" in sequencestr_in:
        print("couldn't append number to image names for frames: format not found")
        sys.exit()
    createdirs(videostr,model)

    #Split into pngs
    # print("Splitting Frames for '"+videostr+"'")
    if os.path.isfile(sequencestr_in.replace("%06d","000001")):
        # print("skipping splitting because already done.")
        return
    # print("converting now...")
    subprocess.run([ffmpegpath,"-loglevel","panic","-nostats","-threads","8","-i",videostr,"-vf","fps="+str(fps),"-q:v","1",sequencestr_in,"-y"])
    tmpstr=""
    for item in [ffmpegpath,"-loglevel","panic","-nostats","-threads","8","-i",videostr,"-vf","fps="+str(fps),"-q:v","1",sequencestr_in,"-y"]:
        tmpstr+=item+" "
    print(tmpstr[:-1]+"\n")
    # print("finished splitting frames for '"+videostr+"'.")

    #Scale Up
    #Clientside
def scaleup(rawname,model,width,height):
    init()
    global ffmpegpath,waifu2xpath,projectpath,cuipath,BITRATE
    global MAXWIDTH,MAXHEIGHT
    validate()
    t=threading.Thread(target=showStatus, args=(rawname,model,), daemon=True)
    t.start()
    used_model=waifu2xpath+os.sep+"models"+os.sep+model
    infolder=projectpath+os.sep+"in"+os.sep+rawname+"^"+model
    outfolder=projectpath+os.sep+"out"+os.sep+rawname+"^"+model
    outstr=outfolder+".mp4"

    sequencestr_in=infolder+os.sep+rawname+"^"+model+" %06d.png"
    sequencestr_out=outfolder+os.sep+rawname+"^"+model+" %06d.png"

    if (width/height)>float(MAXWIDTH)/float(MAXHEIGHT):
        ratio=float(MAXWIDTH)/width
    else:
        ratio=float(MAXHEIGHT)/height

    tmpstr=""
    for item in [cuipath,"--gpu 0","-b","1","-c","128","-q","100","-p","gpu","--model_dir",used_model,"-s",str(ratio),"-n","3","-m","noise_scale","--crop-size","128","-i",""+infolder+"","-l","png","-e","png","-o",""+outfolder+""]:
        tmpstr+=item+" "
    # print(tmpstr[:-1]+"\n")
    subprocess.run([cuipath,"--gpu 0","-b","1","-c","128","-q","100","-p","gpu","--model_dir",used_model,"-s",str(ratio),"-n","3","-m","noise_scale","--crop-size","128","-i",""+infolder+"","-l","png","-e","png","-o",""+outfolder+""])
    # print("finished scaling up '"+rawname+"'")
    t.join()

    #Reconvert to vid
    #Serverside!
def rejoin(videostr,model,fps):
    init()
    global ffmpegpath,waifu2xpath,projectpath,cuipath,BITRATE
    validate()
    # print("videostr:",videostr)
    rawname=videostr.split(os.sep)[-1].split(".")[0]
    # print("rawname:",rawname)
    infolder=projectpath+os.sep+"in"+os.sep+rawname+"^"+model
    # print("infolder:",infolder)
    outfolder=projectpath+os.sep+"out"+os.sep+rawname+"^"+model
    # print("outfolder:",outfolder)
    outstr=projectpath+os.sep+"out"+os.sep+rawname+"."+model+".mp4"
    # print("outstr:",outstr)

    sequencestr_in=infolder+os.sep+rawname+"^"+model+" %06d.png"
    sequencestr_out=outfolder+os.sep+rawname+"^"+model+" %06d.png"

    if os.path.isfile(outstr):
        os.remove(outstr)
    reconvert_cmd=[ffmpegpath, "-loglevel", "panic", "-nostats", "-threads", "8", "-framerate", str(fps), "-i", sequencestr_out, "-i", videostr, "-map", "0:v", "-map", "1:a?", "-c:v", "libx264", "-pix_fmt", "yuv420p", "-b:v", "20M", outstr,"-y"]
    # print("Rejoining Frames for '"+outstr+"'")
    tmpstr=""
    for item in [ffmpegpath, "-loglevel", "panic", "-nostats", "-threads", "8", "-framerate", str(fps), "-i", sequencestr_out, "-i", videostr, "-map", "0:v", "-map", "1:a?", "-c:v", "libx264", "-pix_fmt", "yuv420p", "-b:v", "20M", outstr,"-y"]:
        tmpstr+=item+" "
    # print(tmpstr[:-1]+"\n")
    try:
        stdout=subprocess.check_output([ffmpegpath, "-loglevel", "panic", "-nostats", "-threads", "8", "-framerate", str(fps), "-i", sequencestr_out, "-i", videostr, "-map", "0:v", "-map", "1:a?", "-c:v", "libx264", "-pix_fmt", "yuv420p", "-b:v", "20M", outstr,"-y"])
    except (KeyboardInterrupt):
        return
    except:
        print("didn't work. retrying with even pixel adjustment filter...")
        for item in [ffmpegpath, "-loglevel", "panic", "-nostats", "-threads", "8", "-framerate", str(fps), "-i", sequencestr_out, "-i", videostr, "-map", "0:v", "-map", "1:a?", "-c:v", "libx264", "-pix_fmt", "yuv420p", "-b:v", "20M", "-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2", outstr,"-y"]:
            tmpstr+=item+" "
        # print(tmpstr[:-1]+"\n")
        stdout=subprocess.check_output([ffmpegpath, "-loglevel", "panic", "-nostats", "-threads", "8", "-framerate", str(fps), "-i", sequencestr_out, "-i", videostr, "-map", "0:v", "-map", "1:a?", "-c:v", "libx264", "-pix_fmt", "yuv420p", "-b:v", "20M", "-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2", outstr,"-y"])
        # print("finished rejoining frames for '"+outstr+"'")

    #clearup
def clearup(videostr):
    init()
    # print("placeholder text")
    #do something smart to cleanup
