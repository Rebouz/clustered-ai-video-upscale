DEPENDENCIES=[
"zipfile",
"subprocess",
"multiprocessing",
"urllib",
"hashlib",
"socket",
"shutil",
"threading",
"json",
"datetime",
"secrets"
]

def install_dependencies():
    import sys,os, importlib
    global DEPENDENCIES
    print("Retrieving dependencies...")
    for item in DEPENDENCIES:
        try:
            importlib.import_module(item)
        except:
            os.system("pip install "+item)
    if not test_dependencies():
        sys.exit()

def test_dependencies():
    for item in DEPENDENCIES:
        try:
            exec("import "+item)
        except:
            print(item+"could not be successfully imported!")
            return False
    return True

def create_env():
    import requests,zipfile,subprocess,os,sys,json,secrets,hashlib

    print("Creating Environment...")
    s_code=400
    while s_code!=200:
        try:
            r = requests.get("http://${ip}:${port}/getworkingprogram", timeout=10)
            s_code=r.status_code
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout):
            s_code=500
    with open("working_program.zip","wb") as f:
        f.write(r.content)

    print("unzipping barebones...")
    zipf=zipfile.ZipFile("working_program.zip","r")
    zipf.extractall(os.getcwd()+os.sep+"content")

    print("getting convert config...")
    s_code=400
    while s_code!=200:
        try:
            r = requests.get("http://${ip}:${port}/getconvertconfig", timeout=10)
            s_code=r.status_code
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout):
            s_code=500
    with open("convertconfig.json","wb") as f:
        f.write(r.content)

    print("getting worker config...")
    s_code=400
    while s_code!=200:
        try:
            r = requests.get("http://${ip}:${port}/getworkerconfig", timeout=10)
            s_code=r.status_code
        except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout):
            s_code=500
    with open("worker_config.json","wb") as f:
        f.write(r.content)

    print("Creating folder structure")
    if not os.path.isdir("content"):
        os.mkdir("content")
    if not os.path.isdir("content"+os.sep+"in"):
        os.mkdir("content"+os.sep+"in")
    if not os.path.isdir("content"+os.sep+"out"):
        os.mkdir("content"+os.sep+"out")

    id="000000" #exclude dummy id
    while id=="000000":
        id=str(secrets.randbelow(10000000))

    print("Setting ID to '"+id+"'")
    with open("worker_config.json","w") as f:
        json.dump({"id":id},f) #max 1000 parallel connections

    print("testing environment")
    if not test_env():
        sys.exit()

def test_env():
    import os
    working=True
    working=working and os.path.isfile("autoconvert.py")
    working=working and os.path.isfile("start_workload.py")
    working=working and os.path.isfile("convertconfig.json")
    working=working and os.path.isdir("content"+os.sep+"ffmpeg-latest-win64-static")
    working=working and os.path.isdir("content"+os.sep+"waifu2x-caffe")
    return working

def work():
    import subprocess
    subprocess.call(["python","start_workload.py"])
