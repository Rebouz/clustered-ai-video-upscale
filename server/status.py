import handlerextension, os, urllib, calendar, time, math, psutil

#some functions for disk space display
def getDiskSpace():
    total,used,free,percent=psutil.disk_usage(".")
    return (total*math.pow(10,-9),used*math.pow(10,-9),free*math.pow(10,-9),percent) # all in gigabytes

def getAllData():
    ret_fin=[]
    ret_pend=[]
    ret_preshall=[]
    ret_shall=[]
    general_info={}
    data=handlerextension.getGlobal("WORKLOADS").find({})
    all_finished=0
    ENSURE_VIDID=handlerextension.getGlobal("ENSURE_VIDID")
    CLUSTERSIZE=handlerextension.getGlobal("CLUSTERSIZE")
    general_info["name"]="Overview"
    disk_total,disk_usage,disk_free,disk_percentage=getDiskSpace()

    general_info["disk_total"]=str(int(disk_total))
    general_info["disk_usage"]=str(int(disk_usage))
    general_info["disk_free"]=str(int(disk_free))
    general_info["disk_percentage"]=str(disk_percentage)
    index=0
    for vid in data:
        index+=1
        vid["index"]=index
        framecount,fps,height,width=handlerextension.getProperties(vid["name"])
        maxcls = math.ceil(framecount/handlerextension.getGlobal("CLUSTERSIZE"))
        ret_vid={}
        pending=0
        fin=0
        i=-1
        for cl in vid["clusters"]:
            i+=1
            if cl["status"]=="finished":
                fin+=1
            else:
                pending+=1
        activepicturenum=int(framecount/2)
        if vid["status"]!="shallow":
            activethumb="/in/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+"/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+" "+str(activepicturenum).zfill(6)+".png"
            ret_vid["hasthumb"]=True
        else:
            ret_vid["hasthumb"]=False
            activethumb="/404.svg"
        internal_vidpath=("content"+os.sep+"out"+os.sep+vid["name"].split(os.sep)[-1]).rsplit(".",1)[0]+"."+vid["model"]+"."+vid["name"].rsplit(".",1)[-1]
        if vid["status"]=="finished":
            external_vidpath="/out/"+urllib.parse.quote(internal_vidpath.split(os.sep)[-1])
            ret_vid["vidpath"]=external_vidpath
            ret_vid["hasvid"]=True
        else:
            ret_vid["vidpath"]="#"
            ret_vid["hasvid"]=False


        ret_vid["vidid"]=str(vid["_id"])
        ret_vid["ensuring"]=(vid["status"] in ["splitting","compressing"])
        ret_vid["name"]=vid["name"].split(os.sep)[-1]
        ret_vid["encname"]=urllib.parse.quote(ret_vid["name"].split(".")[0])
        ret_vid["model"]=vid["model"]
        ret_vid["max"]=str(maxcls)
        ret_vid["finished"]=str(fin)
        ret_vid["pending"]=str(pending)
        if vid["status"]=="finished": #completely finished
            all_finished+=1
            ret_vid["status"]="finished"
            ret_vid["thumb"]=activethumb
            ret_fin.append(ret_vid)
        elif not vid["status"] in ["pending"]: #not currently in progress
            if vid["status"] in ["splitting","compressing", "ready"]: #ensuring
                ret_vid["status"]="preshallow"
                ret_vid["thumb"]=activethumb
                ret_preshall.append(ret_vid)
            elif vid["status"]=="shallow": #shallow
                ret_vid["status"]="shallow"
                ret_vid["thumb"]="/404.svg"
                ret_vid["hasthumb"]=False
                ret_shall.append(ret_vid)
            elif vid["status"]=="broken":
                ret_vid["status"]="broken"
                ret_vid["thumb"]="/404.svg"
                ret_vid["hasthumb"]=False
                ret_preshall.append(ret_vid)
        else:
            ret_vid["status"]="pending" #currently in progess (ready)
            ret_vid["thumb"]=activethumb
            ret_pend.append(ret_vid)
    needed=index
    if all_finished==needed:
        general_info["percentage"]="100"
        general_info["isShallow"]=False
        general_info["current"]=str(needed)
        general_info["needed"]=general_info["current"]
    else:
        general_info["percentage"]=str(int((all_finished/needed)*100))
        general_info["isShallow"]=True
        general_info["current"]=str(all_finished)
        general_info["needed"]=str(needed)
    return {"general_info":general_info,"clusters":(ret_pend+ret_preshall+ret_shall+ret_fin)}

def getVidData(vid):
    ret=[]
    ret_shall=[]
    ret_fin=[]
    ret_pend=[]
    general_info={}
    ENSURE_VIDID=handlerextension.getGlobal("ENSURE_VIDID")
    data=handlerextension.getGlobal("WORKLOADS").find({})
    framecount,fps,height,width=handlerextension.getProperties(vid["name"])
    general_info["name"]=vid["name"].split(os.sep)[-1].split(".")[0]

    general_info["ensuring"]=(str(vid["_id"])==ENSURE_VIDID) and handlerextension.getGlobal("ENSURE_RUNNING")
    if not vid["status"] in ["shallow", "splitting"]:
        general_info["hasPreparedClusters"]="true"
    else:
        general_info["hasPreparedClusters"]="false"
    if vid["status"]=="shallow":
        general_info["isShallow"]="true"
        general_info["percentage"]="0"
        general_info["current"]="0"
        general_info["needed"]="?"
        general_info["currentpicture"]="/404"
        return {"general_info": general_info,"clusters": []}
    allfiles=os.listdir("content"+os.sep+"in"+os.sep+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"])
    try:
        allfiles[0]
        current=int(allfiles[-1].split(".")[0].split(" ")[-1])
    except:
        current=0
    maxcls=math.ceil(framecount/handlerextension.getGlobal("CLUSTERSIZE"))
    general_info["max"]=str(maxcls)
    if not os.path.isfile("content"+os.sep+"in"+os.sep+vid["name"].split(".")[0].split(os.sep)[-1]+"^"+vid["model"]+os.sep+str(maxcls-1)+".zip"):
        general_info["percentage"]=str(int((current/int(framecount))*100))
        general_info["isShallow"]="true"
        general_info["current"]=str(current)
        general_info["needed"]=str(int(framecount))
        general_info["currentpicture"]="/in/"+general_info["name"]+"^"+vid["model"]+"/"+general_info["name"]+"^"+vid["model"]+"%20"+general_info["current"].zfill(6)+".png"
    else:
        general_info["percentage"]="100"
        general_info["isShallow"]="false"
        general_info["current"]=str(int(framecount))
        general_info["needed"]=general_info["current"]
        general_info["currentpicture"]="/in/"+general_info["name"]+"^"+vid["model"]+"/"+general_info["name"]+"^"+vid["model"]+"%20"+general_info["current"].zfill(6)+".png"
        internal_vidpath=("content"+os.sep+"out"+os.sep+vid["name"].split(os.sep)[-1]).rsplit(".",1)[0]+"."+vid["model"]+"."+vid["name"].rsplit(".",1)[-1]
        if vid["status"]=="finished":
            external_vidpath="/out/"+urllib.parse.quote(internal_vidpath.split(os.sep)[-1])
            general_info["vidpath"]=external_vidpath
            general_info["hasvid"]=True
        else:
            general_info["vidpath"]="#"
            general_info["hasvid"]=False
    lastindex=-1
    for cluster in vid["clusters"]:
        lastindex=int(cluster["index"])
        cl={}
        cl["type"]="cluster"
        cl["status"]=cluster["status"]
        cl["ip"]=cluster["ip"]
        cl["id"]=cluster["id"]
        imageindex=min(int((float(cluster["index"])*float(handlerextension.getGlobal("CLUSTERSIZE")))+float(handlerextension.getGlobal("CLUSTERSIZE"))/2),framecount)
        cl["thumb"]="/in/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+"/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+" "+str(imageindex).zfill(6)+".png"
        if cluster["status"]=="finished":
            cl["image"]="/out/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+"/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+" "+str(imageindex).zfill(6)+".png"
        else:
            cl["image"]=cl["thumb"]
        cl["name"]=vid["name"].split(os.sep)[-1].split(".")[0]
        cl["encname"]=urllib.parse.quote(""+vid["name"].split(os.sep)[-1])
        cl["model"]=vid["model"]
        cl["index"]=str(cluster["index"])
        if (calendar.timegm(time.gmtime())-int(cluster["timestamp"]))>(60*60):
            cl["timestamp"]=str((calendar.timegm(time.gmtime())-int(cluster["timestamp"]))//(60*60))+" hours ago"
        elif (calendar.timegm(time.gmtime())-int(cluster["timestamp"]))>60:
            cl["timestamp"]=str((calendar.timegm(time.gmtime())-int(cluster["timestamp"]))//60)+" minutes ago"
        else:
            cl["timestamp"]=str(calendar.timegm(time.gmtime())-int(cluster["timestamp"]))+" seconds ago"
        if cl["status"]=="finished":
            ret_fin.append(cl)
        elif cl["status"]=="pending":
            ret_pend.append(cl)
    if lastindex==-1 or not lastindex==math.ceil(framecount/handlerextension.getGlobal("CLUSTERSIZE")):
        for i in range(lastindex+1,math.ceil(framecount/handlerextension.getGlobal("CLUSTERSIZE"))):
            if os.path.isfile("content"+os.sep+"in"+os.sep+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+os.sep+str(i)+".zip"):
                cl={}
                cl["status"]="shallow"
                imageindex=min(int((float(i)*float(handlerextension.getGlobal("CLUSTERSIZE")))+float(handlerextension.getGlobal("CLUSTERSIZE"))/2),framecount)
                cl["thumb"]="/in/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+"/"+vid["name"].split(os.sep)[-1].split(".")[0]+"^"+vid["model"]+" "+str(imageindex).zfill(6)+".png"
                cl["image"]=cl["thumb"]
                cl["encname"]=urllib.parse.quote(""+vid["name"].split(os.sep)[-1])
                cl["model"]=vid["model"]
                cl["index"]=str(i)
                cl["name"]=vid["name"].split(os.sep)[-1].split(".")[0]
                cl["timestamp"]="not started"
                cl["type"]="shallow"
                ret_shall.append(cl)
    return {"general_info":general_info, "clusters":(ret_pend+ret_shall+ret_fin)}
