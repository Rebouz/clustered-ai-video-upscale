import os,socket

try:
    import requests
except:
    os.system("pip install requests")

s_code=400
while s_code!=200:
    try:
        r = requests.get("http://${ip}:${port}/getenv", timeout=10)
        s_code=r.status_code
    except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout):
        s_code=500
with open("setup_env.py","wb") as f:
    f.write(r.content)

s_code=400
while s_code!=200:
    try:
        r = requests.get("http://${ip}:${port}/getworkloadhandler", timeout=10)
        s_code=r.status_code
    except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout):
        s_code=500
with open("start_workload.py","wb") as f:
    f.write(r.content)

s_code=400
while s_code!=200:
    try:
        r = requests.get("http://${ip}:${port}/getautoconvert", timeout=10)
        s_code=r.status_code
    except (requests.exceptions.ConnectionError, TimeoutError, socket.timeout):
        s_code=500
with open("autoconvert.py","wb") as f:
    f.write(r.content)

import setup_env
setup_env.install_dependencies()

setup_env.create_env()
setup_env.work()
