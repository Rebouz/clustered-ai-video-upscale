import json, pymongo, os

def initAddJson():
    global DB_CLIENT, DB, DB_URI, DB_NAME, SOURCEFILES, WORKLOADS, VIDINFOS, VIDEOS
    with open("addData.json","r") as f:
        data=json.load(f)
        VIDEOS=data["VIDEOS"]
        DB_URI=data["DATABASE"]["DB_URI"]
        DB_NAME=data["DATABASE"]["DB_NAME"]
    DB_CLIENT=pymongo.MongoClient(DB_URI)
    DB=DB_CLIENT[DB_NAME]
    SOURCEFILES=DB.sourcefiles
    WORKLOADS=DB.workloads
    VIDINFOS=DB.properties

def initAddGeneratorJson():
    global DB_CLIENT, DB, DB_URI, DB_NAME, SOURCEFILES, WORKLOADS, VIDINFOS, VIDEOS
    with open("addDataGenerator.json","r") as f:
        data=json.load(f)
        VIDEOS=data["VIDEOS"]
        DB_URI=data["DATABASE"]["DB_URI"]
        DB_NAME=data["DATABASE"]["DB_NAME"]
    DB_CLIENT=pymongo.MongoClient(DB_URI)
    DB=DB_CLIENT[DB_NAME]
    SOURCEFILES=DB.sourcefiles
    WORKLOADS=DB.workloads
    VIDINFOS=DB.properties

def fromJson():
    initAddJson()
    global DB_CLIENT, DB, DB_URI, DB_NAME, SOURCEFILES, WORKLOADS, VIDINFOS, VIDEOS
    print(str(WORKLOADS.count())+" Videos in DB.")
    print(str(len(VIDEOS))+" Videos in to be added.")
    workloads_added=0
    for addV in VIDEOS:
        if not WORKLOADS.find({"name":addV["NAME"]}).count()>0 and os.path.isfile(addV["NAME"]): #not in DB
            workloads_added+=1
            newdoc={"name":addV["NAME"],"model":addV["MODEL"],"clusters":[]}
            WORKLOADS.insert_one(newdoc)
    print("added "+str(workloads_added)+" to Workloads.")
    sourcefiles_added=0
    for addV in VIDEOS:
        if not SOURCEFILES.find({"name":addV["NAME"]}).count()>0 and os.path.isfile(addV["NAME"]): #not in DB
            sourcefiles_added+=1
            newdoc={"name":addV["NAME"],"model":addV["MODEL"]}
            SOURCEFILES.insert_one(newdoc)
    print("added "+str(sourcefiles_added)+" to Sourcefiles.")
    DB_CLIENT.close()

def fromJsonGenerator():
    initAddGeneratorJson()
    global DB_CLIENT, DB, DB_URI, DB_NAME, SOURCEFILES, WORKLOADS, VIDINFOS, VIDEOS
    new_videos=[]
    for vp in VIDEOS:
        for item in os.listdir(vp["NAME"]):
            if os.path.isfile(vp["NAME"]+os.sep+item) and os.path.splitext(item)[-1] in [".mp4",".avi",".gif",".flv",".mkv"]:
                new_videos.append({"NAME": vp["NAME"]+os.sep+item, "MODEL": vp["MODEL"]})
    with open("addData.json", "w") as f:
        json.dump({"VIDEOS": new_videos, "DATABASE": {"DB_URI": DB_URI, "DB_NAME": DB_NAME}}, f)
    DB_CLIENT.close()


if __name__=="__main__":
    if os.path.isfile("addData.json"):
        print("writing data in DB...")
        fromJson()
    elif os.path.isfile("addDataGenerator.json"):
        print("generating new addData.json...")
        fromJsonGenerator()
        print("writing data in DB...")
        fromJson()
