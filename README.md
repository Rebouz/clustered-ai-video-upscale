# Clustered AI Video Upscaling
This is a centralized video upscale server. It uses a webserver in at it's base, and any PC can download a client to act as a worker on a pending video file.
Supported modes for video upscaling are "photo" for photorealistic videos and "cunet" for 2D Anime Style Art / 2D Cartoon Graphics.
Workers do the upscaling of frames, the server splits, compresses, extracts, validates and rejoins the video file.
Everything is written in Python3, the frontend uses Bootstrap and a lot of Javascript to compensate for the single page application type of dashboard.
The backend is implement with the use of ffmpeg and Waifu2x, as well as various python modules e.g. for compressing and validating workloads.
All data is fed into a MongoDB connection through Python3, Configuration is saved in various JSON files.
this software is pretty much in a beta state and always will be, as currently I am the only contributor and I won't be able to a ton of bug fixing.

USE WITH CAUTION!

this software is targeted at developers right now, as in order for you to understand it, you need a bit of experience.
No documentation provided as of right now.

## known bugs
- if there's an error when trying to split up a video file, it may cascade and the server won't be able to continue preparing video files properly. Cleaning up after such error requires deletion of the "in/videofile" and "out/videofile" folder and restart of the server. Restart of the workers is recommended.
- Huge frontend load time when displaying bigger numbers of clusters or video files. May lead to inresponsive browser windows or similar. The server has no progressive page loading, as I am far too lazy to implement it right now. Fork if motivated.
- many more that I can't possible figure out myself right now. There's always room for improvement, but not today.